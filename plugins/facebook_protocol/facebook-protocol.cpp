/*
 * %kadu copyright begin%
 * Copyright 2017 Rafał Przemysław Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "facebook-protocol.h"
#include "facebook-protocol.moc"

#include "qfacebook/qfacebook-login-job.h"
#include "qfacebook/qfacebook.h"

#include "contacts/contact-manager.h"
#include "plugin/plugin-injected-factory.h"

FacebookProtocol::FacebookProtocol(Account account, ProtocolFactory *factory) :
		Protocol{account, factory},
		m_facebook{std::make_unique<QFacebook>()}
{
}

FacebookProtocol::~FacebookProtocol()
{
	logout();
}

void FacebookProtocol::login()
{
	auto loginJob = m_facebook->login(account().id(), account().password());
	connect(loginJob, &QFacebookLoginJob::finished, this, &FacebookProtocol::facebookLoginFinished);
}

void FacebookProtocol::facebookLoginFinished(const QFacebookLoginResult &result)
{
	m_facebookSession = result.session;

	switch (result.status)
	{
		case QFacebookLoginStatus::OK:
		{
			auto contacts = contactManager()->contacts(account(), ContactManager::ExcludeAnonymous);
			m_facebookRosterService = pluginInjectedFactory()->makeUnique<FacebookRosterService>(std::move(contacts), *this);
			loggedIn();
			break;
		}

		case QFacebookLoginStatus::Error:
			m_facebookRosterService.reset();
			connectionError();
			break;

		case QFacebookLoginStatus::ErrorInvalidPassword:
			m_facebookRosterService.reset();
			printf("password required\n");
			passwordRequired();
			break;
	}
}

void FacebookProtocol::logout()
{
	loggedOut();
	m_facebookSession = std::experimental::nullopt;
}

void FacebookProtocol::sendStatusToServer()
{
	if (!isConnected() && !isDisconnecting())
		return;
}

QString FacebookProtocol::statusPixmapPath()
{
	return QStringLiteral("facebook");
}
