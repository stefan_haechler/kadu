/*
 * %kadu copyright begin%
 * Copyright 2017 Rafał Przemysław Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qfacebook-download-contacts-delta-job.h"

#include "qfacebook-contact.h"
#include "qfacebook-download-contacts-delta-result.h"
#include "qfacebook-http-api.h"
#include "qfacebook-http-reply.h"
#include "qfacebook-http-request.h"
#include "qfacebook-session.h"

#include <QtCore/QJsonArray>

namespace
{

QByteArray parseRemoved(const QByteArray &removed)
{
	auto parsed = QByteArray::fromBase64(removed).split(':');
	if (parsed.length() != 4)
		return {};

	if (parsed[0] != "contact")
		return {};

	return parsed[2];
}

}

QFacebookDownloadContactsDeltaJob::QFacebookDownloadContactsDeltaJob(QFacebookHttpApi &httpApi, QFacebookSession facebookSession, QByteArray deltaCursor, QObject *parent) :
		QObject{parent},
		m_httpApi{httpApi}
{
	auto reply = m_httpApi.usersQueryDelta(facebookSession.accessToken(), std::move(deltaCursor));
	connect(reply, &QFacebookHttpReply::finished, this, &QFacebookDownloadContactsDeltaJob::replyFinished);
}

QFacebookDownloadContactsDeltaJob::~QFacebookDownloadContactsDeltaJob()
{
}

void QFacebookDownloadContactsDeltaJob::replyFinished(const std::experimental::optional<QJsonObject> &result)
{
	deleteLater();

	if (!result)
		return;

	auto deltas = (*result)["viewer"].toObject()["messenger_contacts"].toObject()["deltas"].toObject();
	auto pageInfo = deltas["page_info"].toObject();
	if (pageInfo["has_next_page"].toBool())
	{
		emit finished(QFacebookDownloadContactsDeltaResult{QFacebookDownloadContactsDeltaStatus::ErrorManyPages, {}, {}, {}});
		return;
	}

	auto nodes = deltas["nodes"].toArray();
	auto changes = std::vector<QJsonValue>{std::begin(nodes), std::end(nodes)};
	auto split = std::partition(std::begin(changes), std::end(changes), [](const QJsonValue &v){
		return v.toObject()["added"].isObject();
	});

	auto friends = std::vector<QJsonValue>{};
	std::copy_if(std::begin(changes), split, std::back_inserter(friends), [](const QJsonValue &v){
		return v.toObject()["added"].toObject()["represented_profile"].toObject()["friendship_status"].toString() == "ARE_FRIENDS";
	});
	std::transform(std::begin(friends), std::end(friends), std::back_inserter(m_added), [](const QJsonValue &v){
		return QFacebookContact::fromJson(v.toObject()["added"].toObject());
	});

	auto allRemoved = std::vector<QByteArray>{};
	std::transform(split, std::end(changes), std::back_inserter(allRemoved), [](const QJsonValue &v){
		return parseRemoved(v.toObject()["removed"].toString().toUtf8());
	});
	std::copy_if(std::begin(allRemoved), std::end(allRemoved), std::back_inserter(m_removed), [](const QByteArray &b){
		return !b.isEmpty();
	});

	emit finished(QFacebookDownloadContactsDeltaResult{
		QFacebookDownloadContactsDeltaStatus::OK,
		pageInfo["end_cursor"].toString().toUtf8(), m_added, m_removed});
}
