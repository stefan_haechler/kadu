/*
 * %kadu copyright begin%
 * Copyright 2017 Rafał Przemysław Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qfacebook-download-contacts-job.h"

#include "qfacebook-contact.h"
#include "qfacebook-download-contacts-result.h"
#include "qfacebook-http-api.h"
#include "qfacebook-http-reply.h"
#include "qfacebook-http-request.h"
#include "qfacebook-session.h"

#include <QtCore/QJsonArray>

QFacebookDownloadContactsJob::QFacebookDownloadContactsJob(QFacebookHttpApi &httpApi, QFacebookSession facebookSession, QObject *parent) :
		QObject{parent},
		m_httpApi{httpApi},
		m_facebookSession{std::move(facebookSession)}
{
	auto reply = m_httpApi.usersQuery(m_facebookSession.accessToken());
	connect(reply, &QFacebookHttpReply::finished, this, &QFacebookDownloadContactsJob::replyFinished);
}

QFacebookDownloadContactsJob::~QFacebookDownloadContactsJob()
{
}

void QFacebookDownloadContactsJob::replyFinished(const std::experimental::optional<QJsonObject> &result)
{
	if (!result)
	{
		deleteLater();
		return;
	}

	auto messengerContacts = (*result)["viewer"].toObject()["messenger_contacts"].toObject();
	auto nodes = messengerContacts["nodes"].toArray();
	auto friends = std::vector<QJsonValue>{};
	std::copy_if(std::begin(nodes), std::end(nodes), std::back_inserter(friends), [](const QJsonValue &v){
		return v.toObject()["represented_profile"].toObject()["friendship_status"].toString() == "ARE_FRIENDS";
	});

	std::transform(std::begin(friends), std::end(friends), std::back_inserter(m_contacts), [](const QJsonValue &v){
		return QFacebookContact::fromJson(v.toObject());
	});

	auto pageInfo = messengerContacts["page_info"].toObject();
	if (!pageInfo["has_next_page"].toBool())
	{
		emit finished(QFacebookDownloadContactsResult{pageInfo["delta_cursor"].toString().toUtf8(), m_contacts});
		deleteLater();
		return;
	}

	auto afterReply = m_httpApi.usersQueryAfter(m_facebookSession.accessToken(), pageInfo["end_cursor"].toString().toUtf8());
	connect(afterReply, &QFacebookHttpReply::finished, this, &QFacebookDownloadContactsJob::replyFinished);
}
