/*
 * %kadu copyright begin%
 * Copyright 2017 Rafał Przemysław Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qfacebook.h"
#include "qfacebook.moc"

#include "qfacebook-download-contacts-delta-job.h"
#include "qfacebook-download-contacts-job.h"
#include "qfacebook-http-api.h"
#include "qfacebook-login-job.h"
#include "qfacebook-session.h"

QFacebook::QFacebook(QObject *parent) :
		QObject{parent},
		m_httpApi{std::make_unique<QFacebookHttpApi>()}
{
}

QFacebook::~QFacebook()
{
}

owned_qptr<QFacebookLoginJob> QFacebook::login(QString userName, QString password)
{
	return make_owned<QFacebookLoginJob>(*m_httpApi, std::move(userName), std::move(password), this);
}

owned_qptr<QFacebookDownloadContactsJob> QFacebook::downloadContacts(QFacebookSession facebookSession)
{
	return make_owned<QFacebookDownloadContactsJob>(*m_httpApi, std::move(facebookSession), this);
}

owned_qptr<QFacebookDownloadContactsDeltaJob> QFacebook::downloadContactsDelta(QFacebookSession facebookSession, QByteArray contactsDelta)
{
	return make_owned<QFacebookDownloadContactsDeltaJob>(*m_httpApi, std::move(facebookSession), std::move(contactsDelta), this);
}
