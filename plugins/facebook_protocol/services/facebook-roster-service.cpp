/*
 * %kadu copyright begin%
 * Copyright 2017 Rafał Przemysław Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "services/facebook-roster-service.h"
#include "services/facebook-roster-service.moc"

#include "qfacebook/qfacebook-contact.h"
#include "qfacebook/qfacebook-download-contacts-delta-job.h"
#include "qfacebook/qfacebook-download-contacts-delta-result.h"
#include "qfacebook/qfacebook-download-contacts-job.h"
#include "qfacebook/qfacebook-download-contacts-result.h"
#include "qfacebook/qfacebook.h"
#include "facebook-account-data.h"
#include "facebook-protocol.h"

#include "buddies/buddy-manager.h"
#include "contacts/contact-manager.h"
#include "roster/roster-entry.h"

#include <QtCore/QTimer>

FacebookRosterService::FacebookRosterService(QVector<Contact> contacts, FacebookProtocol &facebookProtocol) :
		RosterService{std::move(contacts), &facebookProtocol},
		m_facebookProtocol{facebookProtocol}
{
	downloadRoster();
}

FacebookRosterService::~FacebookRosterService()
{
}

void FacebookRosterService::setBuddyManager(BuddyManager *buddyManager)
{
	m_buddyManager = buddyManager;
}

void FacebookRosterService::setContactManager(ContactManager *contactManager)
{
	m_contactManager = contactManager;
}

void FacebookRosterService::downloadRoster()
{
	auto accountData = FacebookAccountData{m_facebookProtocol.account()};
	auto deltaCursor = accountData.deltaCursor();

	if (deltaCursor.isEmpty())
	{
		auto downloadContactsJob = m_facebookProtocol.facebook().downloadContacts(m_facebookProtocol.facebookSession());
		connect(downloadContactsJob, &QFacebookDownloadContactsJob::finished, this, &FacebookRosterService::facebookDownloadContactsFinished);
	}
	else
	{
		auto downloadContactsDeltaJob = m_facebookProtocol.facebook().downloadContactsDelta(m_facebookProtocol.facebookSession(), deltaCursor);
		connect(downloadContactsDeltaJob, &QFacebookDownloadContactsDeltaJob::finished, this, &FacebookRosterService::facebookDownloadContactsDeltaFinished);
	}
}

void FacebookRosterService::downloadFullRoster()
{
	auto accountData = FacebookAccountData{m_facebookProtocol.account()};
	accountData.setDeltaCursor({});
	downloadRoster();
}

void FacebookRosterService::scheduleDownloadRoster()
{
	QTimer::singleShot(60 * 1000, this, &FacebookRosterService::downloadRoster);
}

void FacebookRosterService::facebookDownloadContactsFinished(const QFacebookDownloadContactsResult &downloadContactsResult)
{
	replaceContacts(downloadContactsResult.contacts);

	auto accountData = FacebookAccountData{m_facebookProtocol.account()};
	accountData.setDeltaCursor(downloadContactsResult.deltaCursor);

	scheduleDownloadRoster();
}

void FacebookRosterService::facebookDownloadContactsDeltaFinished(const QFacebookDownloadContactsDeltaResult &downloadContactsDeltaResult)
{
	if (downloadContactsDeltaResult.status == QFacebookDownloadContactsDeltaStatus::ErrorManyPages)
	{
		downloadFullRoster();
		return;
	}

	auto accountData = FacebookAccountData{m_facebookProtocol.account()};
	accountData.setDeltaCursor(downloadContactsDeltaResult.deltaCursor);

	for (auto const &c : downloadContactsDeltaResult.added)
		addContact(c);

	for (auto const &r : downloadContactsDeltaResult.removed)
		removeContact(r);

	scheduleDownloadRoster();
}

void FacebookRosterService::replaceContacts(std::vector<QFacebookContact> newContacts)
{
	auto currentContacts = m_contactManager->contacts(m_facebookProtocol.account());
	for (auto const &c: newContacts)
		addContact(c);

	std::vector<QString> currentIds;
	std::transform(std::begin(currentContacts), std::end(currentContacts), std::back_inserter(currentIds), [](const Contact &c){ return c.id(); });
	std::sort(std::begin(currentIds), std::end(currentIds));

	std::vector<QString> newIds;
	std::transform(std::begin(newContacts), std::end(newContacts), std::back_inserter(newIds), [](const QFacebookContact &c){ return QString::fromUtf8(c.id()); });
	std::sort(std::begin(newContacts), std::end(newContacts), [](const QFacebookContact &x, const QFacebookContact &y){ return x.id() < y.id(); });

	std::vector<QString> toRemove;
	std::set_difference(std::begin(currentIds), std::end(currentIds), std::begin(newIds), std::end(newIds), std::back_inserter(toRemove));
	for (auto const &id : toRemove)
		removeContact(id);
}

void FacebookRosterService::addContact(const QFacebookContact &c)
{
	auto contact = m_contactManager->byId(m_facebookProtocol.account(), c.id(), ActionCreateAndAdd);
	if (!contact || contact == account().accountContact())
		return;

	contact.rosterEntry()->setSynchronizingFromRemote();
	if (contact.isAnonymous()) // contact has anonymous buddy, we should search for other
	{
		contact.setOwnerBuddy(m_buddyManager->byDisplay(c.name(), ActionCreateAndAdd));
		contact.ownerBuddy().setAnonymous(false);
	}
	else
		contact.ownerBuddy().setDisplay(c.name());

	auto buddy = m_buddyManager->byContact(contact, ActionCreateAndAdd);
	m_buddyManager->addItem(buddy);
	contact.rosterEntry()->setSynchronized();

	RosterService::addContact(contact);
}

void FacebookRosterService::removeContact(const QString &id)
{
	auto contact = m_contactManager->byId(m_facebookProtocol.account(), id, ActionReturnNull);
	if (contact)
	{
		m_buddyManager->clearOwnerAndRemoveEmptyBuddy(contact);
		RosterService::removeContact(contact);
		contact.rosterEntry()->setSynchronized();
	}
}
